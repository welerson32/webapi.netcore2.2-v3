namespace ComprasApi.Models
{
    public class ItemsList
    {
        public long Id { get; set; }
        public string Item { get; set; }
        public bool Bought { get; set; }
    }
}
